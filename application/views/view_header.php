<!DOCTYPE html>
<html lang="zh-TW">
  <head>
    <title>購物吧 &mdash; 歡迎來到鞋的城市</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700"> 
    <link rel="stylesheet" href="<?php echo base_url();?>plugins/fonts/icomoon/style.css">

    <link rel="stylesheet" href="<?php echo base_url();?>plugins/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>plugins/css/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url();?>plugins/css/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo base_url();?>plugins/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>plugins/css/owl.theme.default.min.css">


    <link rel="stylesheet" href="<?php echo base_url();?>plugins/css/aos.css">

    <link rel="stylesheet" href="<?php echo base_url();?>plugins/css/style.css">
    <script src="<?php echo base_url();?>plugins/js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url();?>plugins/js/jquery-ui.js"></script>
    <script src="<?php echo base_url();?>plugins/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>plugins/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>plugins/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>plugins/js/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo base_url();?>plugins/js/aos.js"></script>
  </head>

  <body>
  <div class="site-wrap">
    <header class="site-navbar"  role="banner">
      <div class="site-navbar-top">
        <div class="container">
          <div class="row align-items-center">

            <div class="col-6 col-md-4 order-2 order-md-1 site-search-icon text-left" style="border:1px grey solid; border-radius: 30px;">
              <form action="" class="site-block-top-search">
                <span class="icon icon-search2"></span>
                <input type="text" class="form-control border-0" placeholder="請輸入要搜尋的關鍵字" style="height: 30px;">
              </form>
            </div>
            <div class="col-12 mb-3 mb-md-0 col-md-4 order-1 order-md-2 text-center">
              <div class="site-logo">
                <a href="index.html" class="js-logo-clone">Shoppers</a>
              </div>
            </div>

            <div class="col-6 col-md-4 order-3 order-md-3 text-right">
              <div class="site-top-icons">
                <ul>
                  <li><a href="#"><span class="icon icon-person"></span></a></li>
                  <li><a href="#"><span class="icon icon-heart-o"></span></a></li>
                  <li>
                    <a href="cart.html" class="site-cart">
                      <span class="icon icon-shopping_cart"></span>
                      <span class="count">2</span>
                    </a>
                  </li> 
                  <li class="d-inline-block d-md-none ml-md-0"><a href="#" class="site-menu-toggle js-menu-toggle"><span class="icon-menu"></span></a></li>
                </ul>
              </div> 
            </div>

          </div>
        </div>
      </div> 
      <nav class="site-navigation text-right text-md-center" role="navigation">
        <div class="container">
          <ul class="site-menu js-clone-nav d-none d-md-block">
            <!-- <li class="has-children active">
              <a href="index.html">首頁</a>
              <ul class="dropdown">
                <li><a href="#"></a></li>
                <li><a href="#">Menu Two</a></li>
                <li><a href="#">Menu Three</a></li>
                <li class="has-children">
                  <a href="#">Sub Menu</a>
                  <ul class="dropdown">
                    <li><a href="#">Menu One</a></li>
                    <li><a href="#">Menu Two</a></li>
                    <li><a href="#">Menu Three</a></li>
                  </ul>
                </li>
              </ul>
            </li> -->
            <li class="has-children">
              <a href="about.html">導覽</a>
              <ul class="dropdown">
                <li><a href="#">關於我們</a></li>
                <li><a href="#">相關新聞</a></li>
                <li><a href="#">Menu Three</a></li>
              </ul>
            </li>
            <li class="has-children">
              <a href="shop.html">購物商品</a>
              <ul class="dropdown">
                <li><a href="#">男性</a></li>
                <li><a href="#">女性</a></li>
                <li><a href="#">小孩</a></li>
              </ul>
            </li>
            <li><a href="#">Catalogue</a></li>
            <li><a href="#">New Arrivals</a></li>
            <li><a href="contact.html">Contact</a></li>
          </ul>
        </div>
      </nav>
    </header>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="myModalLabel">登入/註冊</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label class="col-sm-12 control-label">帳戶(信箱) 
            <h8 style="margin-left: 20px; font-size: 10px;">還沒註冊? 趕快來</h8>
            <a href="javascript:change_to_register_form();">註冊</a>
            <h8 style="font-size: 10px;">吧</h8>
          </label>

          <div class="col-sm-10">
            <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
          </div>
        </div>
        <div class="form-group">
          <label for="inputPassword3" class="col-sm-12 control-label">密碼</label>
          <div class="col-sm-10">
            <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <div class="checkbox">
              <label>
                <input type="checkbox"> Remember me
              </label>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success">送出</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $("#myModal").modal('show');    
</script>
